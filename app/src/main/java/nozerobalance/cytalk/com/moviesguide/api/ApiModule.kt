package nozerobalance.cytalk.com.moviesguide.api

import dagger.Module
import dagger.Provides

import javax.inject.Singleton

@Module
class ApiModule {
    @Provides
    @Singleton
    fun provideRepository(): Repository = RepositoryImpl()
}
