package nozerobalance.cytalk.com.moviesguide.api

import nozerobalance.cytalk.com.moviesguide.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

import javax.inject.Inject
import java.util.concurrent.TimeUnit

class RepositoryImpl @Inject
constructor() : Repository {

    private val retrofit: Retrofit
        get() = Retrofit.Builder()
            .baseUrl(BuildConfig.TMDB_BASE_API)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    private val interceptor: Interceptor
        get() = ApiInterceptor()

    private val okHttpClient: OkHttpClient
        get() {
            val builder = OkHttpClient.Builder()
            builder.connectTimeout(CONNECT_TIMEOUT_IN_MS.toLong(), TimeUnit.MILLISECONDS)
                .addInterceptor(interceptor)

            if (BuildConfig.DEBUG) {
                val loggingInterceptor = HttpLoggingInterceptor()
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                builder.addInterceptor(loggingInterceptor)
            }

            return builder.build()
        }

    override fun getApi() = retrofit.create(ApiMoviesTMDB::class.java)

    companion object {

        val CONNECT_TIMEOUT_IN_MS = 5000
    }
}
