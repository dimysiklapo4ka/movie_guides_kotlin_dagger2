package nozerobalance.cytalk.com.moviesguide.api.model

import com.google.gson.annotations.SerializedName

/**
 * Created by ivan on 8/20/2017.
 */

data class MoviesWraper(@SerializedName("results")
    var movieList: MutableList<Movie>)
