package nozerobalance.cytalk.com.moviesguide.ui

import nozerobalance.cytalk.com.moviesguide.base.BasePresenter
import nozerobalance.cytalk.com.moviesguide.base.BaseView

interface MainContract{

    interface View: BaseView{

    }

    interface Presenter: BasePresenter<View>{

    }

}