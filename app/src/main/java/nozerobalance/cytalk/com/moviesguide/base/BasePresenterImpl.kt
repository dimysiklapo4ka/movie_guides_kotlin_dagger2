package nozerobalance.cytalk.com.moviesguide.base

open class BasePresenterImpl<V: BaseView>: BasePresenter<V>{

    protected var view: V? = null

    override fun attachView(view: V) {
        this.view = view
    }

    override fun dettachView() {
        view = null
    }

}