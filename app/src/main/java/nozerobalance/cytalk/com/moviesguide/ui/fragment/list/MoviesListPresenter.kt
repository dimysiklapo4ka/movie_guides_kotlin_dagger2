package nozerobalance.cytalk.com.moviesguide.ui.fragment.list

import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import nozerobalance.cytalk.com.moviesguide.api.ApiMoviesTMDB
import nozerobalance.cytalk.com.moviesguide.api.model.Movie
import nozerobalance.cytalk.com.moviesguide.base.BasePresenterImpl
import java.text.SimpleDateFormat
import java.util.*

class MoviesListPresenter: BasePresenterImpl<MoviesListContract.View>(), MoviesListContract.Presenter{

    private var mApi: ApiMoviesTMDB? = null
    private val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    private lateinit var rxDisposable: Disposable

    override fun onPause() {
        if (!rxDisposable.isDisposed) {
            rxDisposable.dispose()
        }
        destroyLifecycle()
    }

    override fun onDestroy() {
        if (!rxDisposable.isDisposed) {
            rxDisposable.dispose()
        }
    }

    private fun destroyLifecycle() {
        view?.hideLoading()
        isLoading = true
    }

    override fun setApi(apiMoviesTMDB: ApiMoviesTMDB) {
        mApi = apiMoviesTMDB
    }

    private var mPopularMovies = 1
    private var mHigestMovies = 1
    private var mNowPlaing = 1
    private var isLoading = true
    override fun getMovies(sorting: Int) {
        if (isLoading) {
            isLoading = false
            when (sorting) {
                2 -> rxDisposable = mApi!!.popularMovies(Locale.getDefault().language, mPopularMovies)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe {view!!.showLoading() }
                    .subscribe({
                            setAdapterData(mPopularMovies, it.movieList)
                            isLoading = true
                            view!!.hideLoading()
                            mPopularMovies++
                            mHigestMovies = 1
                            mNowPlaing = 1
                        }, {view!!.hideLoading()})
                3 -> rxDisposable = mApi!!.highestRatedMovies(Locale.getDefault().language, mHigestMovies)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe { view!!.showLoading() }
                        .subscribe({
                            setAdapterData(mHigestMovies, it.movieList)
                            isLoading = true
                            view!!.hideLoading()
                            mHigestMovies++
                            mPopularMovies = 1
                            mNowPlaing = 1
                        }, {view!!.hideLoading()})
                4 -> {
                    val cal = Calendar.getInstance()
                    rxDisposable = mApi!!.newestMovies(Locale.getDefault().language, sdf.format(cal.time), 50)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe { view!!.showLoading() }
                        .subscribe({ moviesWraper ->
                            view!!.setAdapterData(moviesWraper.movieList)
                            isLoading = true
                            view!!.hideLoading()
                            mPopularMovies = 1
                            mHigestMovies = 1
                            mNowPlaing = 1
                        }, {view!!.hideLoading()})
                }
                6 -> rxDisposable = mApi!!.nowPlaing(Locale.getDefault().language, mNowPlaing, Locale.getDefault().country)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe { view!!.showLoading() }
                        .subscribe({ moviesWraper ->
                            setAdapterData(mHigestMovies, moviesWraper.movieList)
                            isLoading = true
                            view!!.hideLoading()
                            mHigestMovies = 1
                            mPopularMovies = 1
                            mNowPlaing++
                        }, {view!!.hideLoading()})
            }
        }
    }

    override fun attachView(view: MoviesListContract.View) {
        this.view = view
    }

    override fun dettachView() {
        view = null
    }

    private fun setAdapterData(rate: Int, movies: MutableList<Movie>) {
        if (rate > 1) {
            Log.e("####", "setAdapterData: $rate")
            view!!.addAdapterData(movies)
        } else {
            Log.e("@@@@", "setAdapterData: $rate")
            view!!.setAdapterData(movies)
        }
    }
}