package nozerobalance.cytalk.com.moviesguide.app.di.component

import dagger.Component
import dagger.Module
import nozerobalance.cytalk.com.moviesguide.api.ApiModule
import nozerobalance.cytalk.com.moviesguide.app.MoviesGuideApp
import nozerobalance.cytalk.com.moviesguide.ui.MainActivity
import nozerobalance.cytalk.com.moviesguide.ui.di.MainActivityModule
import javax.inject.Singleton

@Singleton
@Component(modules = [ApiModule::class, MainActivityModule::class])
interface MoviesGuideComponent{

    fun inject(application: MoviesGuideApp)

    fun inject(mainActivity: MainActivity)

}