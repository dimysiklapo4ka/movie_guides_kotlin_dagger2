package nozerobalance.cytalk.com.moviesguide.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import nozerobalance.cytalk.com.moviesguide.R

abstract class BaseFragment<V: BaseView, P: BasePresenter<V>>: Fragment(), BaseView{

    protected var swipeRefreshLayout: SwipeRefreshLayout? = null
    protected abstract var mPresenter: P?

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mPresenter?.attachView(this as V)
        return swapUpToSwipeRefreshLayout(inflater.inflate(getLayoutId(), container, false))
    }

    override fun onDestroyView() {
        mPresenter?.dettachView()
        super.onDestroyView()
//        mPresenter = null
    }

    private fun swapUpToSwipeRefreshLayout(view: View):View?{

        swipeRefreshLayout = SwipeRefreshLayout(activity!!)
        swipeRefreshLayout!!.addView(view)
        swipeRefreshLayout!!.setColorSchemeResources(R.color.colorAccent)
        swipeRefreshLayout!!.isRefreshing = false

        return swipeRefreshLayout
    }

    override fun showLoading() {
        swipeRefreshLayout?.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefreshLayout?.isRefreshing = false
    }

    protected abstract fun getLayoutId(): Int
}