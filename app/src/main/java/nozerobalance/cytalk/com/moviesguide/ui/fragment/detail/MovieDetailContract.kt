package nozerobalance.cytalk.com.moviesguide.ui.fragment.detail

import nozerobalance.cytalk.com.moviesguide.api.ApiMoviesTMDB
import nozerobalance.cytalk.com.moviesguide.api.model.Movie
import nozerobalance.cytalk.com.moviesguide.api.model.Video
import nozerobalance.cytalk.com.moviesguide.base.BasePresenter
import nozerobalance.cytalk.com.moviesguide.base.BaseView

interface MovieDetailContract{

    interface View: BaseView{
        fun setAdapterData(movies: MutableList<Video>)
    }

    interface Preesenter: BasePresenter<View>{
        fun onPause()
        fun onDestroy()

        fun setApi(apiMoviesTMDB: ApiMoviesTMDB)
        fun getTrailers(movie: Movie)
    }

}