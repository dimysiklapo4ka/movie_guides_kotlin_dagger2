package nozerobalance.cytalk.com.moviesguide.ui.fragment.list

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SnapHelper
import android.view.View
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_movies.*
import nozerobalance.cytalk.com.moviesguide.R
import nozerobalance.cytalk.com.moviesguide.api.Repository
import nozerobalance.cytalk.com.moviesguide.api.model.Movie
import nozerobalance.cytalk.com.moviesguide.base.BaseFragment
import nozerobalance.cytalk.com.moviesguide.preference.SharedPreferencesData
import nozerobalance.cytalk.com.moviesguide.ui.MainActivity
import nozerobalance.cytalk.com.moviesguide.ui.fragment.list.adapter.MoviesAdapter
import nozerobalance.cytalk.com.moviesguide.ui.fragment.list.recyclerview.BackgroundImage
import nozerobalance.cytalk.com.moviesguide.ui.fragment.list.recyclerview.RecyclerSnapHelper
import nozerobalance.cytalk.com.moviesguide.ui.fragment.listeners.LayoutManagerListener
import nozerobalance.cytalk.com.moviesguide.ui.fragment.listeners.MovieItemClickListener
import nozerobalance.cytalk.com.moviesguide.ui.fragment.listeners.PaginationLoadMoreListener
import javax.inject.Inject

const val LINEAR_MANAGER = 0
const val GRID_MANAGER = 1
const val SINGLE_MANAGER = 5

const val POPULAR_MOVIE = 2
const val HIGHEST_MOVIE = 3
const val NEWEST_MOVIE = 4
const val NOW_PLAING = 6

class MoviesListFragment @Inject constructor(): BaseFragment<MoviesListContract.View, MoviesListContract.Presenter>(),
    MoviesListContract.View, LayoutManagerListener, PaginationLoadMoreListener, BackgroundImage {

    override var mPresenter: MoviesListContract.Presenter? = MoviesListPresenter()
    override fun getLayoutId() = R.layout.fragment_movies

    @Inject lateinit var mApi: Repository
    @Inject lateinit var mData: SharedPreferencesData

    private var linearManager: LinearLayoutManager? = null
    private var gridManager: GridLayoutManager? = null
    private var horizontalManager: LinearLayoutManager? = null

    private var snapHelper: SnapHelper? = null
    private var mAdapter: MoviesAdapter? = null

    private lateinit var mListener: MovieItemClickListener

    private var layoutManager: Int = 0
    private var filter: Int = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        init()
        mPresenter?.setApi(mApi.getApi())
        initRecycler()
        mPresenter?.getMovies(filter)
        swipeRefreshLayout!!.setOnRefreshListener { mPresenter?.getMovies(filter) }
    }

    private fun init(){
        mListener = context as MainActivity

        layoutManager = mData.layoutManager
        filter = mData.filter

        linearManager = LinearLayoutManager(context)
        gridManager = GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false)
        horizontalManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        snapHelper = RecyclerSnapHelper(this)

        mAdapter = MoviesAdapter(this)
        mAdapter!!.setItemClickListener(mListener)
    }

    private fun initRecycler() {
        setLayoutMamager(layoutManager)
        snapHelper!!.attachToRecyclerView(moviesRecycler)
    }

    override fun setLayoutMamager(manager: Int) {
        when (manager) {
            LINEAR_MANAGER -> invalidateList(manager, linearManager!!)
            GRID_MANAGER -> invalidateList(manager, gridManager!!)
            SINGLE_MANAGER -> {
                invalidateList(manager, horizontalManager!!)
                setImage(0)
            }
            else -> {
                mPresenter!!.getMovies(manager)
                mData.filter = manager
                filter = manager
            }
        }
    }

    private fun saveLayoutManager(manager: Int) {
        mData.layoutManager = manager
    }

    private fun invalidateList(manager: Int, layoutManager: RecyclerView.LayoutManager) {
        mAdapter!!.setTypeLayoutManager(manager)
        moviesRecycler.layoutManager = layoutManager
        moviesRecycler.adapter = mAdapter
        saveLayoutManager(manager)
    }

    override fun setAdapterData(movies: MutableList<Movie>) {
        mAdapter!!.setMovies(movies)
    }

    override fun addAdapterData(movies: MutableList<Movie>) {
        mAdapter!!.addMovies(movies)
    }

    override fun loadMoreMovies() {
        mPresenter!!.getMovies(filter)
    }

    override fun setImage(position: Int) {
        if (position != -1) {
            Glide.with(this)
                .load(mAdapter!!.getMovies(position))
                .into(ll_root)
            ll_root.visibility = View.VISIBLE
        } else {
            ll_root.visibility = View.INVISIBLE
        }
    }
}