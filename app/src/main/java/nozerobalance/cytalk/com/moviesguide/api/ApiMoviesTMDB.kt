package nozerobalance.cytalk.com.moviesguide.api

import io.reactivex.Observable

import nozerobalance.cytalk.com.moviesguide.api.model.MoviesWraper
import nozerobalance.cytalk.com.moviesguide.api.model.ReviewsWrapper
import nozerobalance.cytalk.com.moviesguide.api.model.VideoWrapper
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiMoviesTMDB {

    @GET(POPULAR_MOVIE)
    fun popularMovies(
        @Query("language") language: String,
        @Query("page") page: Int
    ): Observable<MoviesWraper>

    @GET(HIGHEST_RATED_MOVIES)
    fun highestRatedMovies(
        @Query("language") language: String,
        @Query("page") page: Int
    ): Observable<MoviesWraper>

    @GET(NEWEST_MOVIES)
    fun newestMovies(
        @Query("language") language: String,
        @Query("release_date.lte") maxReleaseDate: String,
        @Query("vote_count.gte") minVoteCount: Int
    ): Observable<MoviesWraper>

    @GET(TRAILERS)
    fun trailers(@Path("movieId") movieId: String): Observable<VideoWrapper>

    @GET(REVIEWS)
    fun reviews(@Path("movieId") movieId: String): Observable<ReviewsWrapper>

    @GET(SEARCH_MOVIES)
    fun searchMovies(
        @Query("language") language: String,
        @Query("query") searchQuery: String
    ): Observable<MoviesWraper>

    @GET(NOW_PLAING)
    fun nowPlaing(
        @Query("language") language: String,
        @Query("page") page: Int,
        @Query("region") region: String
    ): Observable<MoviesWraper>

}
