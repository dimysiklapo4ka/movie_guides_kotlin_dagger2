package nozerobalance.cytalk.com.moviesguide.ui

import nozerobalance.cytalk.com.moviesguide.base.BasePresenterImpl

class MainPresenter: BasePresenterImpl<MainContract.View>(), MainContract.Presenter{

    override fun attachView(view: MainContract.View) {
        this.view = view
    }

    override fun dettachView() {
        view = null
    }
}