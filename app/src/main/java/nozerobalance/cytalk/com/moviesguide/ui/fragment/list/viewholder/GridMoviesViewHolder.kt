package nozerobalance.cytalk.com.moviesguide.ui.fragment.list.viewholder

import android.graphics.Bitmap
import android.support.v7.graphics.Palette
import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.bumptech.glide.request.transition.Transition
import kotlinx.android.synthetic.main.view_grid_movie_item.view.*
import nozerobalance.cytalk.com.moviesguide.R
import nozerobalance.cytalk.com.moviesguide.api.ApiConstants
import nozerobalance.cytalk.com.moviesguide.api.model.Movie
import nozerobalance.cytalk.com.moviesguide.ui.fragment.listeners.MovieItemClickListener

class GridMoviesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var mListener: MovieItemClickListener? = null
    private var movie: Movie? = null

    init {
        itemView.setOnClickListener(Clicked())
    }

    fun bind(movie: Movie, listener: MovieItemClickListener?) {
        mListener = listener
        this.movie = movie
        bindView(movie)
    }

    private fun bindView(movie: Movie) {
        itemView.textView.text = movie.title
        Glide.with(itemView.context)
            .asBitmap()
            .load(ApiConstants.getPosterPath(movie.posterPath))
            .into(object : BitmapImageViewTarget(itemView.imageView) {
                override fun onResourceReady(bitmap: Bitmap, transition: Transition<in Bitmap>?) {
                    super.onResourceReady(bitmap, transition)
                    Palette.from(bitmap).generate { palette -> setBackgroundColor(palette!!) }
                }
            })
    }

    private fun setBackgroundColor(palette: Palette) {
        itemView.textView.setBackgroundColor(
            palette.getVibrantColor(
                itemView.context
                    .resources.getColor(R.color.black_translucent_60)
            )
        )
        itemView.textView.alpha = 0.8f
    }

    private inner class Clicked : View.OnClickListener {

        override fun onClick(v: View) {
            mListener!!.movieClicked(movie!!)
        }
    }
}
