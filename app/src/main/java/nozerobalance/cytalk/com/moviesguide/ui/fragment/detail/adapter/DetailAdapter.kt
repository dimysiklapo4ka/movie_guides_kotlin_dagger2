package nozerobalance.cytalk.com.moviesguide.ui.fragment.detail.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import nozerobalance.cytalk.com.moviesguide.R
import nozerobalance.cytalk.com.moviesguide.api.model.Video
import nozerobalance.cytalk.com.moviesguide.ui.fragment.detail.viewholder.DetailViewHolder

import java.util.ArrayList

class DetailAdapter : RecyclerView.Adapter<DetailViewHolder>() {

    private var mVideos: List<Video>? = null

    init {
        mVideos = ArrayList<Video>()
    }

    fun setVideos(videos: List<Video>?) {
        if (videos != null) {
            mVideos = videos
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): DetailViewHolder {
        return DetailViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.view_trailer, viewGroup, false))
    }

    override fun onBindViewHolder(detailViewHolder: DetailViewHolder, i: Int) {
        detailViewHolder.bind(mVideos!![i])
    }

    override fun getItemCount(): Int {
        return mVideos!!.size
    }
}
