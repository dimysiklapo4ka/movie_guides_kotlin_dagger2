package nozerobalance.cytalk.com.moviesguide.app

import android.app.Application
import nozerobalance.cytalk.com.moviesguide.api.ApiModule
import nozerobalance.cytalk.com.moviesguide.app.di.component.DaggerMoviesGuideComponent
import nozerobalance.cytalk.com.moviesguide.app.di.component.MoviesGuideComponent
import nozerobalance.cytalk.com.moviesguide.ui.di.MainActivityModule

class MoviesGuideApp: Application() {

    companion object {
       @JvmStatic lateinit var daggerComponent: MoviesGuideComponent
    }

    override fun onCreate() {
        super.onCreate()

        daggerComponent = DaggerMoviesGuideComponent.builder()
            .mainActivityModule(MainActivityModule(this))
            .apiModule(ApiModule())
            .build()

        daggerComponent.inject(this)
    }
}