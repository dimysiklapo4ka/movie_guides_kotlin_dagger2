package nozerobalance.cytalk.com.moviesguide.api

const val BASE_POSTER_PATH = "https://image.tmdb.org/t/p/w342"
const val BASR_BACKDROP_PATH = "https://image.tmdb.org/t/p/w780"
const val YOUTUBE_VIDEO_URL = "https://www.youtube.com/watch?v=%1\$s"
const val YOUTUBE_THUMBNAIL_URL = "https://img.youtube.com/vi/%1\$s/0.jpg"

const val POPULAR_MOVIE = "discover/movie?language=en&sort_by=popularity.desc"
const val HIGHEST_RATED_MOVIES = "discover/movie?vote_count.gte=500&language=en&sort_by=vote_average.desc\""
const val NEWEST_MOVIES = "discover/movie?language=en&sort_by=release_date.desc"
const val TRAILERS = "movie/{movieId}/videos"
const val REVIEWS = "movie/{movieId}/reviews"
const val SEARCH_MOVIES = "search/movie?language=en-US&page=1"
const val NOW_PLAING = "movie/now_playing"

object ApiConstants {

    fun getPosterPath(posterPath: String): String {
        return BASE_POSTER_PATH + posterPath
    }

    fun getBackdropPath(backdropPath: String): String {
        return BASR_BACKDROP_PATH + backdropPath
    }
}
