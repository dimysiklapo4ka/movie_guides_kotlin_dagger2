package nozerobalance.cytalk.com.moviesguide.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity<in V: BaseView, P: BasePresenter<V>>: AppCompatActivity(), BaseView{

    abstract var mPresenter: P?

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        init()
        mPresenter?.attachView(this as V)
    }

    override fun onDestroy() {
        mPresenter?.dettachView()
        super.onDestroy()
        mPresenter = null
    }

    protected abstract fun getLayoutId(): Int

    protected abstract fun init()
}