package nozerobalance.cytalk.com.moviesguide.ui.fragment.list.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import nozerobalance.cytalk.com.moviesguide.R
import nozerobalance.cytalk.com.moviesguide.api.ApiConstants
import nozerobalance.cytalk.com.moviesguide.api.model.Movie
import nozerobalance.cytalk.com.moviesguide.ui.fragment.list.GRID_MANAGER
import nozerobalance.cytalk.com.moviesguide.ui.fragment.list.LINEAR_MANAGER
import nozerobalance.cytalk.com.moviesguide.ui.fragment.list.SINGLE_MANAGER
import nozerobalance.cytalk.com.moviesguide.ui.fragment.list.viewholder.GridMoviesViewHolder
import nozerobalance.cytalk.com.moviesguide.ui.fragment.list.viewholder.LinearMoviesViewHolder
import nozerobalance.cytalk.com.moviesguide.ui.fragment.list.viewholder.SingleMoviesViewHolder
import nozerobalance.cytalk.com.moviesguide.ui.fragment.listeners.MovieItemClickListener
import nozerobalance.cytalk.com.moviesguide.ui.fragment.listeners.PaginationLoadMoreListener

import java.util.ArrayList

class MoviesAdapter(private val mLoadMoreListener: PaginationLoadMoreListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mMovies: MutableList<Movie>? = ArrayList()
    private var TYPE_LAYOUT_MANAGER = 1
    private var mListener: MovieItemClickListener? = null

    fun setMovies(movies: MutableList<Movie>) {
        if (movies.isNotEmpty()) {
            mMovies = movies
            notifyDataSetChanged()
        }
    }

    fun addMovies(movies: MutableList<Movie>?) {
        if (movies != null && movies.isNotEmpty()) {
            mMovies!!.addAll(movies)
            notifyDataSetChanged()
        }
    }

    fun getMovies(position: Int): String? {
        return if (mMovies != null && mMovies!!.size != 0) {
            ApiConstants.getPosterPath(mMovies!![position].posterPath)
        } else null
    }

    fun setTypeLayoutManager(layoutManager: Int) {
        TYPE_LAYOUT_MANAGER = layoutManager
        notifyDataSetChanged()
    }

    fun setItemClickListener(listener: MovieItemClickListener) {
        mListener = listener
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        return when (TYPE_LAYOUT_MANAGER) {
            LINEAR_MANAGER -> return LinearMoviesViewHolder(createView(viewGroup, R.layout.view_linear_movie_item))
            GRID_MANAGER -> return GridMoviesViewHolder(createView(viewGroup, R.layout.view_grid_movie_item))
            SINGLE_MANAGER -> return SingleMoviesViewHolder(createView(viewGroup, R.layout.view_single_movie_item))
            else -> LinearMoviesViewHolder(createView(viewGroup, R.layout.view_linear_movie_item))
        }
    }

    private fun createView(viewGroup: ViewGroup, resLayout: Int) =
        LayoutInflater.from(viewGroup.context).inflate(resLayout, viewGroup, false)

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        when (viewHolder) {
            is LinearMoviesViewHolder -> viewHolder.bind(mMovies!![i], mListener)
            is GridMoviesViewHolder -> viewHolder.bind(mMovies!![i], mListener)
            is SingleMoviesViewHolder -> viewHolder.bind(mMovies!![i], mListener)
        }
        if (i == mMovies!!.size - 1) mLoadMoreListener.loadMoreMovies()
    }

    override fun getItemCount(): Int {
        return mMovies!!.size
    }
}
