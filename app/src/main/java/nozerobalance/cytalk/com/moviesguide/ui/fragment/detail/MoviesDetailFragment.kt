package nozerobalance.cytalk.com.moviesguide.ui.fragment.detail

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_detail_movie.*
import nozerobalance.cytalk.com.moviesguide.R
import nozerobalance.cytalk.com.moviesguide.api.ApiConstants
import nozerobalance.cytalk.com.moviesguide.api.Repository
import nozerobalance.cytalk.com.moviesguide.api.model.Movie
import nozerobalance.cytalk.com.moviesguide.api.model.Video
import nozerobalance.cytalk.com.moviesguide.base.BaseFragment
import nozerobalance.cytalk.com.moviesguide.ui.MainActivity
import nozerobalance.cytalk.com.moviesguide.ui.fragment.detail.adapter.DetailAdapter
import nozerobalance.cytalk.com.moviesguide.ui.fragment.listeners.MovieItemClickListener
import javax.inject.Inject

class MoviesDetailFragment @Inject constructor(): BaseFragment<MovieDetailContract.View, MovieDetailContract.Preesenter>(), MovieDetailContract.View,
                            MovieItemClickListener{

    private var movie: Movie? = null
    private val mAdapter: DetailAdapter? = DetailAdapter()

    @Inject
    lateinit var mApi: Repository

    override fun setAdapterData(movies: MutableList<Video>) {
        mAdapter!!.setVideos(movies)
    }

    override var mPresenter: MovieDetailContract.Preesenter? = MovieDetailPresenter()
    override fun getLayoutId() = R.layout.fragment_detail_movie

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipeRefreshLayout?.isEnabled = false
        mPresenter!!.setApi(mApi.getApi())
        initTrailers()
        setData()
        mPresenter!!.getTrailers(movie!!)
    }

    private fun initTrailers() {
        trailers.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        trailers.adapter = mAdapter
    }

    private fun setData() {
        if (movie != null) {
            Glide.with(activity!!)
                .load(ApiConstants.getBackdropPath(movie!!.backdropPath))
                .into(detail_poster)
            detail_name_movie.text = movie!!.title
            movie_year.text = "Release Date: ${movie!!.releaseDate}"
            movie_rating.text = "Rating movies : ${movie!!.voteAverage}/10"
            detail_description_movie.text = movie!!.overview
        }
    }

    override fun movieClicked(movie: Movie) {
        this.movie = movie
    }

    fun onBackPresed(activity: Activity) {
        val act = activity as MainActivity
        act.replaceMovieFragm()
    }
}