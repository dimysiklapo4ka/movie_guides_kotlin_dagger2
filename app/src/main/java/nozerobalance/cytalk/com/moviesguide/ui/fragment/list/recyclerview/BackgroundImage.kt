package nozerobalance.cytalk.com.moviesguide.ui.fragment.list.recyclerview

interface BackgroundImage {
    fun setImage(position: Int)
}
