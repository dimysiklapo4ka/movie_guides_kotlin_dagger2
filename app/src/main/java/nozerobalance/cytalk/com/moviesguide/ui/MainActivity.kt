package nozerobalance.cytalk.com.moviesguide.ui

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.view_toolbar.*
import nozerobalance.cytalk.com.moviesguide.R
import nozerobalance.cytalk.com.moviesguide.api.model.Movie
import nozerobalance.cytalk.com.moviesguide.app.MoviesGuideApp
import nozerobalance.cytalk.com.moviesguide.base.BaseActivity
import nozerobalance.cytalk.com.moviesguide.replaceFragment
import nozerobalance.cytalk.com.moviesguide.ui.fragment.detail.MoviesDetailFragment
import nozerobalance.cytalk.com.moviesguide.ui.fragment.list.*
import nozerobalance.cytalk.com.moviesguide.ui.fragment.list.MoviesListFragment
import nozerobalance.cytalk.com.moviesguide.ui.fragment.listeners.LayoutManagerListener
import nozerobalance.cytalk.com.moviesguide.ui.fragment.listeners.MovieItemClickListener
import javax.inject.Inject
import javax.inject.Named

class MainActivity @Inject constructor(): BaseActivity<MainContract.View, MainContract.Presenter>(), MainContract.View
                    , MovieItemClickListener{

    override var mPresenter: MainContract.Presenter? = MainPresenter()

    @Inject
    lateinit var movieListFragment: MoviesListFragment
//    @field:[Inject Named("MoviesDetailFragment")]
    @Inject
    lateinit var movieDetailFragment: MoviesDetailFragment

    private lateinit var mListener: LayoutManagerListener
    private lateinit var mSendMovie: MovieItemClickListener

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_toolbar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_manager -> AlertDialog.Builder(this)
                .setTitle("LayoutManager")
                .setMessage("check layout manager")
                .setPositiveButton("Linear") { _, _ -> mListener.setLayoutMamager(LINEAR_MANAGER) }
                .setNegativeButton("Grid") { _, _ -> mListener.setLayoutMamager(GRID_MANAGER) }
                .setNeutralButton("Single") { _, _ -> mListener.setLayoutMamager(SINGLE_MANAGER) }
                .create()
                .show()
            R.id.action_popular -> mListener.setLayoutMamager(POPULAR_MOVIE)
            R.id.action_highest -> mListener.setLayoutMamager(HIGHEST_MOVIE)
            R.id.action_newest -> mListener.setLayoutMamager(NEWEST_MOVIE)
            R.id.action_now_playing -> mListener.setLayoutMamager(NOW_PLAING)
        }
        return true
    }

    override fun onBackPressed() {

        movieDetailFragment.onBackPresed(this)
    }

    override fun showLoading() {

    }

    override fun hideLoading() {

    }

    fun replaceMovieFragm() {
        replaceFragment(R.id.mainContainer, movieListFragment, false)
        toolbar.visibility = View.VISIBLE
    }

    override fun init() {
        MoviesGuideApp.daggerComponent.inject(this)
        setSupportActionBar(toolbar)
        replaceFragment(R.id.mainContainer, movieListFragment, false)

        mListener = movieListFragment
        mSendMovie = movieDetailFragment
    }

    override fun movieClicked(movie: Movie) {
        replaceFragment(R.id.mainContainer, movieDetailFragment, false)
        mSendMovie.movieClicked(movie)
        toolbar.visibility = View.GONE
    }

    override fun getLayoutId() = R.layout.activity_main
}
