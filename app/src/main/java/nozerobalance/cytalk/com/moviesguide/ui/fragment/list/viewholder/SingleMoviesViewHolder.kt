package nozerobalance.cytalk.com.moviesguide.ui.fragment.list.viewholder

import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_single_movie_item.view.*
import nozerobalance.cytalk.com.moviesguide.api.ApiConstants
import nozerobalance.cytalk.com.moviesguide.api.model.Movie
import nozerobalance.cytalk.com.moviesguide.ui.MainActivity
import nozerobalance.cytalk.com.moviesguide.ui.fragment.listeners.MovieItemClickListener

class SingleMoviesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var movie: Movie? = null
    private var mListener: MovieItemClickListener? = null

    init {
        itemView.setOnClickListener(Clicked())

        val displayMetrics = DisplayMetrics()
        (itemView.context as MainActivity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        val widths = (width * 0.7f).toInt()
        val height = ViewGroup.LayoutParams.MATCH_PARENT

        itemView.layoutParams = ViewGroup.LayoutParams(widths, height)
    }

    fun bind(movie: Movie, listener: MovieItemClickListener?) {
        this.movie = movie
        mListener = listener
        bindView(movie)
    }

    private fun bindView(movie: Movie) {
        Glide.with(itemView.context)
            .load(ApiConstants.getPosterPath(movie.posterPath))
            .into(itemView.iv_single_poster)
    }

    private inner class Clicked : View.OnClickListener {

        override fun onClick(v: View) {
            mListener!!.movieClicked(movie!!)
        }
    }
}
