package nozerobalance.cytalk.com.moviesguide.ui.di

import android.content.Context
import android.support.v4.app.Fragment
import dagger.Binds
import dagger.Module
import dagger.Provides
import nozerobalance.cytalk.com.moviesguide.app.MoviesGuideApp
import nozerobalance.cytalk.com.moviesguide.app.di.scoupe.ActivityScope
import nozerobalance.cytalk.com.moviesguide.app.di.scoupe.FragmentScope
import nozerobalance.cytalk.com.moviesguide.preference.SharedPreferencesData
import nozerobalance.cytalk.com.moviesguide.preference.SharedPreferencesDataImpl
import nozerobalance.cytalk.com.moviesguide.ui.fragment.detail.MoviesDetailFragment
import nozerobalance.cytalk.com.moviesguide.ui.fragment.list.MoviesListFragment
import javax.inject.Named
import javax.inject.Singleton

@Module
class MainActivityModule(private val application: MoviesGuideApp){

    @Provides
    fun injectContext(): Context = application.applicationContext

    @FragmentScope
    @Provides
    fun injectMoviesListFragment() : Fragment = MoviesListFragment()

    @FragmentScope
    @Provides
    fun injectMoviesDetailFragment() : Fragment = MoviesDetailFragment()

    @Provides
    @Singleton
    fun injectSharedPreferences(context: Context): SharedPreferencesData = SharedPreferencesDataImpl(context)
}