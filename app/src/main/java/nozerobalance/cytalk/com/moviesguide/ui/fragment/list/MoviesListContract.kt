package nozerobalance.cytalk.com.moviesguide.ui.fragment.list

import nozerobalance.cytalk.com.moviesguide.api.ApiMoviesTMDB
import nozerobalance.cytalk.com.moviesguide.api.model.Movie
import nozerobalance.cytalk.com.moviesguide.base.BasePresenter
import nozerobalance.cytalk.com.moviesguide.base.BaseView

interface MoviesListContract{
    interface View: BaseView{
        fun setAdapterData(movies: MutableList<Movie>)
        fun addAdapterData(movies: MutableList<Movie>)
    }

    interface Presenter: BasePresenter<View>{
        fun onPause()
        fun onDestroy()

        fun setApi(apiMoviesTMDB: ApiMoviesTMDB)
        fun getMovies(sorting: Int)
    }
}