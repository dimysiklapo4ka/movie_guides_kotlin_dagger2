package nozerobalance.cytalk.com.moviesguide.ui.fragment.detail

import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import nozerobalance.cytalk.com.moviesguide.api.ApiMoviesTMDB
import nozerobalance.cytalk.com.moviesguide.api.model.Movie
import nozerobalance.cytalk.com.moviesguide.base.BasePresenterImpl

class MovieDetailPresenter: BasePresenterImpl<MovieDetailContract.View>(), MovieDetailContract.Preesenter{

    private var mApi: ApiMoviesTMDB? = null
    private var disposable: Disposable? = null

    override fun onPause() {
        if (!disposable?.isDisposed!!) {
            disposable?.dispose()
        }
    }

    override fun onDestroy() {
        if (!disposable?.isDisposed!!) {
            disposable?.dispose()
        }
    }

    override fun setApi(apiMoviesTMDB: ApiMoviesTMDB) {
        mApi = apiMoviesTMDB
    }

    override fun getTrailers(movie: Movie) {
        disposable = mApi!!.trailers(movie.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ videos -> view!!.setAdapterData(videos.videos) },
                { loging(it) })
    }

    override fun attachView(view: MovieDetailContract.View) {
        this.view = view
    }

    override fun dettachView() {
        view = null
    }

    private fun loging(throwable: Throwable) {
        Log.e(MovieDetailPresenter::class.java.simpleName, throwable.message, throwable)
        view!!.hideLoading()
    }
}