package nozerobalance.cytalk.com.moviesguide.api

interface Repository {
    fun getApi(): ApiMoviesTMDB
}
