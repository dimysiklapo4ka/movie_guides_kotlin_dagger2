package nozerobalance.cytalk.com.moviesguide.ui.fragment.listeners

interface PaginationLoadMoreListener {
    fun loadMoreMovies()
}
