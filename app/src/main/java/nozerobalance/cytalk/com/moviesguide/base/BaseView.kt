package nozerobalance.cytalk.com.moviesguide.base

interface BaseView{
    fun showLoading()
    fun hideLoading()
}