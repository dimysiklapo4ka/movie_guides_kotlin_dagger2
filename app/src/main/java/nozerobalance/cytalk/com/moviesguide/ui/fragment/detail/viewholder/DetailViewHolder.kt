package nozerobalance.cytalk.com.moviesguide.ui.fragment.detail.viewholder

import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_detail_movie.view.*
import kotlinx.android.synthetic.main.view_trailer.view.*
import nozerobalance.cytalk.com.moviesguide.api.model.Video

class DetailViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

    private var video: Video? = null

    init {
        itemView.setOnClickListener(this)
    }

    fun bind(video: Video) {
        this.video = video
        bindView(video)
    }

    private fun bindView(video: Video) {
        Glide.with(itemView.context)
            .load(Video.getThumbnailUrl(video))
            .into(itemView.video)
    }

    override fun onClick(v: View) {
        val playVideoIntent = Intent(Intent.ACTION_VIEW, Uri.parse(Video.getUrl(video!!)))
        itemView.context.startActivity(playVideoIntent)
    }
}
