package nozerobalance.cytalk.com.moviesguide.api

import nozerobalance.cytalk.com.moviesguide.BuildConfig
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

import javax.inject.Inject
import java.io.IOException

class ApiInterceptor @Inject
constructor() : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {

        val request = chain.request()

        val originUrl = request.url()

        val url = originUrl.newBuilder()
            .addQueryParameter("api_key", BuildConfig.TMDB_API_KEY)
            .build()

        return chain.proceed(request.newBuilder().url(url).build())
    }
}
