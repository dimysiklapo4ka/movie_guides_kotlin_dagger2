package nozerobalance.cytalk.com.moviesguide.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import javax.inject.Inject;

public class SharedPreferencesDataImpl implements SharedPreferencesData{

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private final String PREFERENCES_NAME = "movies_guide_pref";

    private final String LAYOUT_MANAGER = "LAYOUT_MANAGER";
    private final String FILTER = "FILTER";

    @Inject
    public SharedPreferencesDataImpl(Context context){
        Log.e(SharedPreferencesDataImpl.class.getSimpleName(), "INIT");
        sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    @Override
    public Integer getLayoutManager() {
        Log.e(SharedPreferencesDataImpl.class.getSimpleName(), "GET_LAYOUT_MANAGER");
        return sharedPreferences.getInt(LAYOUT_MANAGER, 1);
    }

    @Override
    public void setLayoutManager(Integer layoutManagerId) {
        Log.e(SharedPreferencesDataImpl.class.getSimpleName(), "set LAYOUT_MANAGER " + layoutManagerId);
        editor.putInt(LAYOUT_MANAGER, layoutManagerId).apply();
    }

    @Override
    public Integer getFilter() {
        Log.e(SharedPreferencesDataImpl.class.getSimpleName(), "GET_FILTER");
        return sharedPreferences.getInt(FILTER, 2);
    }

    @Override
    public void setFilter(Integer filterId) {
        Log.e(SharedPreferencesDataImpl.class.getSimpleName(), "set FILTER " + filterId);
        editor.putInt(FILTER, filterId).apply();
    }
}
