package nozerobalance.cytalk.com.moviesguide.ui.fragment.list.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View

import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_linear_movie_item.view.*
import nozerobalance.cytalk.com.moviesguide.api.ApiConstants
import nozerobalance.cytalk.com.moviesguide.api.model.Movie
import nozerobalance.cytalk.com.moviesguide.ui.fragment.listeners.MovieItemClickListener

class LinearMoviesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var mListener: MovieItemClickListener? = null
    private var movie: Movie? = null

    init {
        itemView.setOnClickListener(Clicked())
    }

    fun bind(movie: Movie, listener: MovieItemClickListener?) {
        mListener = listener
        this.movie = movie
        bindView(movie)
    }

    private fun bindView(movie: Movie) {
        itemView.linearNameMovie.text = movie.title
        itemView.linearDescriptionMovie.text = movie.overview
        Glide.with(itemView.context)
            .load(ApiConstants.getPosterPath(movie.posterPath))
            .into(itemView.linearPosterMovie)
    }

    private inner class Clicked : View.OnClickListener {

        override fun onClick(v: View) {
            mListener!!.movieClicked(movie!!)
        }
    }
}
