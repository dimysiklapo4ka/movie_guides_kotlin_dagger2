package nozerobalance.cytalk.com.moviesguide.ui.fragment.listeners

import nozerobalance.cytalk.com.moviesguide.api.model.Movie

interface MovieItemClickListener {
    fun movieClicked(movie: Movie)
}
