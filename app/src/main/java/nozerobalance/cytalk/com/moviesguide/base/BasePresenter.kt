package nozerobalance.cytalk.com.moviesguide.base

interface BasePresenter<in V:BaseView>{

    fun attachView(view: V)
    fun dettachView()
}